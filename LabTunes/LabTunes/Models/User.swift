//
//  User.swift
//  LabTunes
//
//  Created by Gomez Luis on 11/9/18.
//  Copyright © 2018 Gomez Luis. All rights reserved.
//

import Foundation

class User {
    static let userName = "Luis"
    static let password = "1234"
    static let session = Session.shared
    
    static func login(userName: String, password: String) -> Bool{
        if self.userName == userName {
            session.saveSession()
            return true
        }
        return false
    }
    static func fetchSongs() throws {
        guard let token = session.token else {
            throw UserError.notSessionAvailable
        }
        debugPrint(token)
    }
    
    enum UserError: Error{
        case notSessionAvailable
    }
    
}
