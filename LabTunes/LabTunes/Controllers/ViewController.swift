//
//  ViewController.swift
//  LabTunes
//
//  Created by Gomez Luis on 11/9/18.
//  Copyright © 2018 Gomez Luis. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didLogin(_ sender: Any) {
        guard let userName = usernameTextField.text else{return}
        guard let password = passwordTextField.text else{return}
        if User.login(userName: userName, password: password){
            performSegue(withIdentifier: "MusicTableSegue", sender: self)
        }
    }
    
}

