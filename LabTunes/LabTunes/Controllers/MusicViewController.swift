//
//  MusicViewController.swift
//  LabTunes
//
//  Created by Gomez Luis on 11/10/18.
//  Copyright © 2018 Gomez Luis. All rights reserved.
//

import UIKit

class MusicViewController: UIViewController{

    @IBOutlet weak var tableView: UITableView!
    
    var songs = [Song]()
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadSongs()
        setupSearchBar()
    }
    func downloadSongs(){
        Music.fetchSongs {[weak self] (songs) in
            self?.songs = songs
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }
    func setupSearchBar(){
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Search songs"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    func downloadSongsBy(name: String){
        Music.fetchSongs(songName: name) {[weak self] (songs: [Song]) in
            self?.songs = songs
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }
}

extension MusicViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MusicCell", for: indexPath)
        let song = songs[indexPath.row]
        cell.textLabel?.text = song.name
        cell.detailTextLabel?.text = song.artist
        return cell
    }
}

extension MusicViewController: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        guard let songName = searchController.searchBar.text else {return}
        if songName != ""{
            downloadSongsBy(name: songName)
        }
    }
}
