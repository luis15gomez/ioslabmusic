//
//  LabTunesTests.swift
//  LabTunesTests
//
//  Created by Gomez Luis on 11/9/18.
//  Copyright © 2018 Gomez Luis. All rights reserved.
//

import XCTest
@testable import LabTunes

class LabTunesTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        let session = Session.shared
        session.token = nil
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCorrectLogin(){
        XCTAssertTrue(User.login(userName: "Luis", password: "1234"))
    }
    
    func testWrongLogin(){
        XCTAssertFalse(User.login(userName: "Lukjis", password: "1234"))
    }
    
    func testSaveSession(){
        let session = Session.shared
        let _ = User.login(userName: "Luis", password: "1234")
        XCTAssertNotNil(session.token)
    }
    
    func testSessionNil(){
        let session = Session.shared
        let _ = User.login(userName: "Ldfuis", password: "1234")
        XCTAssertNil(session.token)
    }
    
    func testExpectedToken(){
        let _ = User.login(userName: "Luis", password: "223")
        let session =  Session.shared
        XCTAssertEqual(session.token!, "1234567890", "Token should Match")
    }
    
    func testNotExpectedToken(){
        let _ = User.login(userName: "Lusis", password: "223")
        let session =  Session.shared
        XCTAssertNotEqual(session.token, "1234567890", "Token shouldnt Match")
    }
    
    func testFetchSongs(){
        let _ = User.login(userName: "Lusdfsdfsis", password: "223")
        XCTAssertThrowsError(try User.fetchSongs())
    }
    func testMusicSongs(){
        var resultSongs: [Song] = []
        let promise = expectation(description: "Songs")
        Music.fetchSongs { (songs) in
            resultSongs = songs
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotEqual(resultSongs.count, 0)
    }
    
    
}
